﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MyLib
{
    public static class ServiceConfiguration
    {
        public static void Configure(IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            IConfiguration configuration = serviceProvider.GetService<IConfiguration>();
            IConfigurationSection mySection = configuration.GetSection("SubEntry:My");
            
            // this does not work, although it works in the main project
            
            // services.Configure<MyOptions>(mySection);
        }
    }
}
