using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace OptionsReproduction
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            MyLib.ServiceConfiguration.Configure(services);

            //ServiceProvider serviceProvider = services.BuildServiceProvider();
            //IConfiguration configuration = serviceProvider.GetService<IConfiguration>();
            //IConfigurationSection mySection = configuration.GetSection("MySection");
            
            // The code down below is fine but it is invalid syntax in MyLib
            
            //services.Configure<MyLib.MyOptions>(mySection);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
